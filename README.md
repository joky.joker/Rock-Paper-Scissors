# Rock-Paper-Scissors

## Design Ex1

* CommandLineParams.h
* Config.h
* DisplayManager.h
* FightResult.h
* FileUtil.h
* Game.h
* GameBoard.h
* GameBoardCell.h
* LosingReason.h
* MoveFileParser.h
* MoveOperation.h
* Piece.h
* PieceType.h
* Player.h
* PositionFileParser.h


Position
```<PIECE_CHAR> <X> <Y>
J <X> <Y> <PIECE_CHAR>```

Move format
`<FROM_X> <FROM_Y> <TO_X> <TO_Y> [J: <Joker_X> <Joker_Y> <NEW_REP>]`


