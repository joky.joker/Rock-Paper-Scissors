#pragma once
#include "Player.h"
#include <ostream>

using namespace std;
class Player;

enum class Reason {
	ALL_FLAGS_CAPTURED,
	ALL_MOVING_PIECES_EATEN,
	BAD_POSITIONING_FILE,
	BAD_MOVES_FILE,
	DIDNT_LOSE,
};

class LosingReason {
	int m_wrongLine = 0;
	Reason m_reason = Reason::DIDNT_LOSE;
public:
	LosingReason() = default;
	LosingReason(Reason reason, int wrong_line = 0) : m_reason(reason), m_wrongLine(wrong_line) {}
	Reason getReason() { return m_reason; }
	int getWrongLine() { return m_wrongLine; }
};
