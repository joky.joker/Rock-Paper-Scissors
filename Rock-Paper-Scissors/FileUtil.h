#pragma once
#include <fstream>
#include <iostream>

class FileUtil {
	int m_lineNumber = 1;
	std::ifstream m_file;
public:
	~FileUtil() { m_file.clear();m_file.close(); }
	static bool isFileExists(char * file_name);
	bool isOpen() { return m_file.is_open(); }
	bool isGood() { return m_file.good(); }
	bool isDone();
	void unget() { m_file.unget(); }
	bool isFailed();
	bool openFile(char * file_path);
	int getLineNumber() { return m_lineNumber;}
	char getNextChar();
	char getNextByte() { return m_file.get(); }
	int getNextNumber();
	int consumeChar(char ch);
	void consumeSpaces() { consumeChar(' '); }
	void consumeNewLines() { m_lineNumber += consumeChar('\n'); }
	void consumeWhiteSpaces();
};