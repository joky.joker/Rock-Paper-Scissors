#pragma once


enum FightResult { 
	WIN, 
	LOSE, 
	BOTH_DIE 
};