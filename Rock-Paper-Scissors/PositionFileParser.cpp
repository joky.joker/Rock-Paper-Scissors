#include "PositionFileParser.h"
#include <string>
#include <cmath>

PositionResult PositionFileParser::parseNextPositionPiece()
{
	int xpos, ypos;
	PositionResult result;
	result.piece.setType(PieceType::INVALID);
	if (!m_fileUtil.isOpen() || !m_fileUtil.isGood()) {
		cout << "ERROR: invalid filestream given" << endl;
		return result;
	}
	m_fileUtil.consumeSpaces();
	m_fileUtil.consumeNewLines();

	char charType = m_fileUtil.getNextChar();
	//NOTE: reached the end of file
	if (charType == istream::traits_type::eof()) {
		return result;
	}

	PieceType type = Piece::parsePieceType(charType);
	if (type == PieceType::INVALID) {
		cerr << "ERROR: parsed invalid piece type" << endl;
		return result;
	}

	PieceType pieceType = (PieceType)(type);
	charType = pieceType;
	xpos = m_fileUtil.getNextNumber() - 1;
	ypos = m_fileUtil.getNextNumber() - 1;
	if (pieceType == PieceType::JOKER) {
		charType = m_fileUtil.getNextChar();
		if (!Piece::isRPSB(charType)) {
			cerr << "ERROR: Invalid Joker type '" << charType << "' at line: " << getLineNumber() << endl;
			return result;
		}
		result.piece.setJoker(true);
	}
	pieceType = Piece::parsePieceType(charType);
	if (pieceType == PieceType::INVALID) {
		cerr << "ERROR: parsed invalid piece type "<< (char)pieceType << " at line:" << getLineNumber() << endl;
		return result;
	}

	if (!GameBoard::verifyPointRange(xpos, ypos)) {
		cerr << "Invalid piece position (" << xpos + 1 << "," << ypos + 1 << ") at line: " << getLineNumber() << endl;
		return result;
	}
	m_fileUtil.consumeSpaces();
	result.piece.setType(pieceType);
	result.x = xpos;
	result.y = ypos;
	return result;
}
