#include "CommandLineParams.h"
#include <cstring>
#include <string>
#include <iostream>

const char CommandLineParams::PARAM_QUIET[] = "-quiet";
const char CommandLineParams::PARAM_SHOW_ALL[] = "-show-all";
const char CommandLineParams::PARAM_SHOW_ONLY_KNOWN_INFO[] = "-show-only-known-info";
const char CommandLineParams::PARAM_SHOW[] = "-show";
const char CommandLineParams::PARAM_DELAY[] = "-delay";

CommandLineParams CommandLineParams::parseCommandLineParams(int argc, char ** argv)
{
	CommandLineParams params;
	//Comparison order is critical, because -show-all can match on -show
	for (int i = 1; i < argc; i++) {
		char * arg = argv[i];
		if (std::strcmp(arg, CommandLineParams::PARAM_QUIET) == 0) {
			params.m_presentation = false;
		}
		else if (std::strcmp(arg, CommandLineParams::PARAM_SHOW_ONLY_KNOWN_INFO) == 0) {
			params.m_showOnlyKnownInfo = true;
		} 
		else if (std::strcmp(arg, CommandLineParams::PARAM_SHOW_ALL) == 0) {
			params.m_showAll = true;
		// (i < argc -1)  -> To make sure we are not at the end of the argument line
		} 
		else if ((std::strcmp(arg, CommandLineParams::PARAM_SHOW) == 0) && (i < argc - 1)) {
			params.m_showPlayer = std::atoi(argv[++i]);
		} 
		else if ((std::strcmp(arg, CommandLineParams::PARAM_DELAY) == 0) && (i < argc - 1)) {
			params.m_delay = std::atoi(argv[++i]);
		} 
		else {
			std::cout << "Unknown parameter '" << arg << "'" << std::endl;
		}
	}
	return params;
}

int CommandLineParams::getShow() const
{
	if (isPresentationMode()) {
		return m_showPlayer;
	}
	return 0;
}

int CommandLineParams::getDelay() const
{
	if (isPresentationMode()) {
		return m_delay;
	}
	return 0;
}
