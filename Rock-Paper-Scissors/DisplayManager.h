#pragma once
#include "CommandLineParams.h"
#include "GameBoardCell.h"

class GameBoard;
enum Color {
	BLACK,
	BLUE,
	GREEN,
	CYAN,
	RED,
	MAGENTA,
	BROWN,
	LIGHTGRAY,
	DARKGRAY,
	LIGHTBLUE,
	LIGHTGREEN,
	LIGHTCYAN,
	LIGHTRED,
	LIGHTMAGENTA,
	YELLOW,
	WHITE,
};
class DisplayManager {
	CommandLineParams m_params;
	int m_startingX = 0;
	int m_startingY = 0;
	int m_endingX = 0;
	int m_endingY = 0;
	static constexpr char SEPERATOR = '|';
	void printPieceCell(Piece& piece);
	void printPiece(Piece& piece);
	void printPieceShowAll(Piece& piece);
	void printPieceShowOnlyKnownInfo(Piece& piece);
	void getCursorXY(int& x, int& y);
	void printLine();
	static void resetColor() { setColor(); }
	static void setColor(Color ForgC = Color::WHITE, Color BackC = Color::BLACK);
	static void moveCursorToPosition(int x, int y);
public:
	DisplayManager() = default;
	explicit DisplayManager(const CommandLineParams& params) : m_params(params) {};
	void printGameBoard(GameBoard& board);
	void onLocationUpdated(GameBoardCell& cell, int x, int y);
	void delay();
	Color playerIDToColor(int playerID) { return (Color)(playerID + 2); }
	void printInColor(const char* strToPrint, Color fgColor = Color::WHITE, Color bgColor = Color::BLACK);
};