#include "Game.h"
#include "Player.h"
#include "PieceTypeCounter.h"

Player * Game::playerIDToPlayer(int playerID)
{
	if (playerID == 0) {
		return nullptr;
	}
	return &this->m_players[playerID - 1];
}

Game::Game(const CommandLineParams& params, int numOfPlayers, int numOfRounds):
	m_params(params), m_numOfRounds(numOfRounds), m_numOfPlayers(numOfPlayers)
{
	this->m_players = new Player[numOfPlayers];
	for (int i = 0; i < numOfPlayers; i++) {
		this->m_players[i].setPlayerID(i + 1);
	}
	m_display = DisplayManager(params);
	m_gameBoard = GameBoard(this->m_players, &m_display, m_numOfPlayers);
}

Game::~Game()
{
	delete[] this->m_players;
	this->m_players = nullptr;
}

int Game::getWinner(bool isInitialCheck)
{
	int didntLose = 0;
	int lastWinner = 0;
	for (int i = 0; i < this->m_numOfPlayers; i++) {
		if (!isInitialCheck) {
			if (this->m_players[i].hasLost()) {
				continue;
			}
		}
		else if (this->m_players[i].hasFlagBeenCaptured()) {
			continue;
		}
		
		lastWinner = this->m_players[i].getPlayerID();
		didntLose++;
	}
	//Everyone lost
	if (didntLose == 0) {
		return 0;
	}

	//Only 1 player didnt lose - therefore he wins
	if (didntLose == 1) {
		return lastWinner;
	}
	//Otherwise, its unknown - no winner
	return -1;
}

int Game::Run()
{
	int highScore = 0;
	Player * winner = nullptr;

	for (int i = 0; i < this->m_numOfRounds; i++) {
		int playerID = RunRound();
		if (playerID == 0 || playerID == -1) {
			continue;
		}
		Player * player = this->playerIDToPlayer(playerID);
		player->addPoint();
		if (player->getScore() > highScore) {
			highScore = player->getScore();
			winner = player;
		}
	}

	if (winner == nullptr) {
		dumpIntoOutputFile(0);
		return 0;
	}
	dumpIntoOutputFile(winner->getPlayerID());
	return winner->getPlayerID();
}

int Game::RunRound()
{
	this->m_gameBoard.reduceAllCells();
	int winner = getWinner(true);
	if (winner != -1) {
		//Someone won or everyone lost.
		return winner;
	}
	int playersPlaying = m_numOfPlayers;
	m_display.printInColor("Rock Paper Scissors Game! Ex1");
	cout << endl;
	m_display.printInColor("Player 1", m_display.playerIDToColor(1));
	cout << ", ";
	m_display.printInColor("Player 2", m_display.playerIDToColor(2));
	cout << endl;
	m_display.printGameBoard(m_gameBoard);
	//Run as long as someone plays
	while (playersPlaying >= 1) {
		playersPlaying = 0;
		for (int i = 0; i < this->m_numOfPlayers; i++) {
			if (this->m_players[i].hasLost()) {
				continue;
			}

			//Check if the last joker transformation caused a winning situation
			// ex: transformed his last joker into a bomb (unmoveable therefore loses)
			winner = this->getWinner();
			if (winner != -1) {
				return winner;
			}
			
			Player& p = this->m_players[i];
			MoveFileParser& moveFile = p.getPlayerMoveFile();
			if (moveFile.isFailed() || !moveFile.isOpen()) {
				cerr << "Move file could not be properly open for player " << p.getPlayerID() << endl;
			}
			if (moveFile.isDone()) {
				continue;
			}
			
			m_display.delay();
			//Treat move and joker transformation as two different actions
			MoveOperation operation = moveFile.nextMoveOperation();
			if (!this->m_gameBoard.applyPlayerMoveOperation(p, operation)) {
				cerr << "Player " << p.getPlayerID() << " has lost due to invalid move file at line: " << moveFile.getLineNumber() << endl;
				p.lose(LosingReason(Reason::BAD_MOVES_FILE, moveFile.getLineNumber()));
				continue;
			}
			playersPlaying += 1;

			//Check if the last move caused a winning situation
			winner = this->getWinner();
			if (winner != -1) {
				return winner;
			}

			if (!this->m_gameBoard.applyPlayerJokerTransformationOperation(p, operation)) {
				cerr << "Player " << p.getPlayerID() << " has lost due to invalid move file at line: " << moveFile.getLineNumber() << endl;
				p.lose(LosingReason(Reason::BAD_MOVES_FILE, moveFile.getLineNumber()));
				continue;
			}

		}
	}
	//Nobody won.
	return getWinner();
}

int Game::initilizeGamePositions()
{
	int playersPlaying = this->m_numOfPlayers;
	int didntLose = 0;
	for (int i = 0; i < this->m_numOfPlayers; i++) {
		bool hasLost = this->initilizePositiningFile(this->m_players[i]);
		if (hasLost) {
			cout << "Player: " << this->m_players[i].getPlayerID() << " has lost" << endl;
			playersPlaying -= 1;
			continue;
		}
		didntLose = this->m_players[i].getPlayerID();
	}
	if (playersPlaying == 0) {
		dumpIntoOutputFile(0);
		return 0;
	}
	if (playersPlaying == 1) {
		dumpIntoOutputFile(didntLose);
		return didntLose;
	}
	return -1;
}

bool Game::validateFilesExistance()
{
	bool result = validatePositiningFilesExistance();
	return result && validateMoveFilesExistance();
}

bool Game::validateFilesExistance(char * file_template)
{
	char * file_name = file_template;
	constexpr int NUM_INDEX = 6;
	bool isOkay = true;
	for (int i = 1; i <= this->m_numOfPlayers; i++) {
		file_name[NUM_INDEX] = i + '0';
		if (!FileUtil::isFileExists(file_name)) {
			cerr << "ERROR: Missing file '" << file_name << "'" << endl;
			isOkay = false;
		}
	}
	return isOkay;
}

bool Game::validateMoveFilesExistance()
{
	char move_file_name[] = PLAYER_MOVE_FILE_NAME;
	return validateFilesExistance(move_file_name);
}

bool Game::validatePositiningFilesExistance()
{
	char position_file_name[] = PLAYER_POS_FILE_NAME;
	return validateFilesExistance(position_file_name);
}

void Game::dumpIntoOutputFile(int winner)
{
	ofstream file(OUTPUT_FILE_NAME);
	file << "Winner: " << winner << endl;
	file << "Reason: ";
	Player::getWinningReason(file, m_players[0], m_players[1]);
	file << endl;
	file << this->m_gameBoard;
}

bool Game::initilizePositiningFile(Player & player)
{
	char file_name[] = PLAYER_POS_FILE_NAME;
	constexpr int NUM_INDEX = 6;
	file_name[NUM_INDEX] = player.getPlayerID() + '0';
	if (loadPlayerPositionFile(file_name, player)) {
		return player.hasLost();
	}
	return true;
}

bool Game::loadPlayerPositionFile(char * filePath, Player & player)
{
	//TODO: add rollback in case of more than 1 player.
	PositionFileParser parser;
	PieceTypeCounter counter;
	if (!parser.openFile(filePath)) {
		cerr << "Player " << player.getPlayerID() << " Position file could not be opened" << endl;
		player.lose(Reason::BAD_POSITIONING_FILE);
		return false;
	}
	while (!parser.isDone() && !parser.isFailed()) {
		PositionResult result = parser.parseNextPositionPiece();
		if (result.piece.getType() == PieceType::INVALID) {
			cerr << "Player " << player.getPlayerID() << " has lost due to invalid piece at line:" << parser.getLineNumber() << endl;
			player.lose(LosingReason(Reason::BAD_POSITIONING_FILE, parser.getLineNumber()));
			return false;
		}
		result.piece.setPlayer(&player);
		Piece * addedPiece = this->m_gameBoard.initilizePieceAtLocation(result.piece, result.x, result.y);
		if (addedPiece == nullptr) {
			cerr << "Player " << player.getPlayerID() << " has lost due to invalid positioning at line:" << parser.getLineNumber() << endl;
			player.lose(LosingReason(Reason::BAD_POSITIONING_FILE, parser.getLineNumber()));
			return false;
		}

		if (!player.addPiece(*addedPiece)) {
			cerr << "Player " << player.getPlayerID() << " has lost due to piece max capacity exceeding at line:" << parser.getLineNumber() << endl;
			player.lose(LosingReason(Reason::BAD_POSITIONING_FILE, parser.getLineNumber()));
			return false;
		}

		if (!counter.countPiece(*addedPiece)) {
			cerr << "Player " << player.getPlayerID() << " has lost due to piece max capacity exceeding of Type " << (char)result.piece.getType() << endl;
			player.lose(LosingReason(Reason::BAD_POSITIONING_FILE, parser.getLineNumber()));
			return false;
		}
	}
	if (!counter.validateAllIncludingFlags()) {
		cerr << "Player " << player.getPlayerID() << " has lost due to invalid number of pieces " << endl;
		player.lose(LosingReason(Reason::BAD_POSITIONING_FILE, parser.getLineNumber() + 1));
		return false;
	}
	if (parser.isFailed()) {
		cerr << "Player " << player.getPlayerID() << " has lost because positioning file read operation has failed (line:" << parser.getLineNumber() << ")"<< endl;
		player.lose(LosingReason(Reason::BAD_POSITIONING_FILE, parser.getLineNumber()));
		return false;
	}
	return true;
}
