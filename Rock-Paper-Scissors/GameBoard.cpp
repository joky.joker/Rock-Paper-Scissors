#include <fstream>
#include <iostream>
#include "GameBoard.h"
#include <cassert>
using namespace std;


bool GameBoard::validatePlayerMoveOperation(Player & player, MoveOperation & operation)
{
	if (!operation.isValidMove()) {
		return false;
	}
	if (!validateSourcePieceBelongsToPlayer(player, operation.getSourceX(), operation.getSourceY())) {
		return false;
	}
	//If the destination has piece of the same player
	if (this->m_board[operation.getDestenationX()][operation.getDestenationY()].isPlayerPresent(player.getPlayerID())) {
		cerr << "ERROR: player " << player.getPlayerID() << "has lost due to attempt to move over his own piece" << endl;
		return false;
	}

	Piece * piece = getPieceAtLocation(operation.getSourceX(), operation.getSourceY());
	if (piece == nullptr || !piece->canMove()) {
		cerr << "Player " << player.getPlayerID() << " has lost due to attempt to move an unmoveable piece on board" << endl;
		return false;
	}
	
	return true;
}

bool GameBoard::validatePlayerJokerTransformationOperation(Player & player, MoveOperation & operation)
{
	if (!operation.isJokerTrasformative()) {
		return true;
	}

	if (!operation.isValidTransformation()) {
		return false;
	}
	
	int jokerX = operation.getJokerX();
	int jokerY = operation.getJokerY();

	if (!validateSourcePieceBelongsToPlayer(player, jokerX, jokerY)) {
		return false;
	}
	Piece * piece = getPieceAtLocation(jokerX, jokerY);
	if (piece == nullptr || !piece->isJoker()) {
		cerr << "Player " << player.getPlayerID() << " has lost due to attempt to modify non Joker piece" << endl;
		return false;
	}

	return true;
}

bool GameBoard::validateSourcePieceBelongsToPlayer(Player & player, int x, int y)
{
	if (!m_board[x][y].isPlayerPresent(player.getPlayerID())) {
		cerr << "Player " << player.getPlayerID() << " has lost due to attempting to modify not owned piece" << endl;
		return false;
	}
	//calling function validated the x,y
	Piece * piece = getPieceAtLocation(x, y);
	if (piece == nullptr) {
		cerr << "ERROR: piece was nullptr after validation, SHOULD NEVER HAPPEN" << endl;
		return false;
	}
	if (piece->isDead()) {
		cerr << "Player " << player.getPlayerID() << " has lost due to attempt to modify a dead piece on board" << endl;
		return false;
	}
	return true;
}

Piece * GameBoard::getPieceAtLocation(int x, int y)
{
	if (!GameBoard::verifyPointRange(x, y)) {
		return nullptr;
	}
	GameBoardCell& cell = this->m_board[x][y];
	if (cell.isEmpty()) {
		return nullptr;
	}
	return &cell.getPiece();
}

GameBoard & GameBoard::operator=(GameBoard && other)
{
	if (this == &other) {
		return *this;
	}
	this->m_players = other.m_players;
	this->m_numOfPlayers = other.m_numOfPlayers;
	this->m_board = other.m_board;
	this->m_display = other.m_display;
	other.m_board = nullptr;
	other.m_numOfPlayers = 0;
	other.m_players = nullptr;
	return *this;
}

GameBoard::~GameBoard()
{
	if (this->m_board == nullptr) {
		return;
	}
	for (int x = 0; x < BoardSize::COLOMNS; x++) {
		delete[] m_board[x];
	}
	delete[] m_board;
	m_board = nullptr;
}


GameBoard::GameBoard(Player * players, DisplayManager* displayManager, int numOfPlayers) :
	m_players(players), m_numOfPlayers(numOfPlayers), m_display(displayManager)
{
	m_board = new GameBoardCell*[BoardSize::COLOMNS];
	for (int i = 0; i < BoardSize::COLOMNS; i++) {
		m_board[i] = new GameBoardCell[BoardSize::ROWS];
	}
}

//called only at initialization
Piece * GameBoard::initilizePieceAtLocation(Piece & piece, int x, int y)
{
	if (!GameBoard::verifyPointRange(x, y)) {
		return nullptr;
	}

	//Check if this player has a piece in this cell already.
	GameBoardCell& cell = m_board[x][y];
	if (cell.isPlayerPresent(piece.getPlayer()->getPlayerID())) {
		//TODO: Maybe player.lose() here?
		return nullptr;
	}
	
	m_board[x][y].allocate(m_numOfPlayers);
	Piece * addedPiece = m_board[x][y].addPiece(piece);
	if (addedPiece == nullptr) {
		return nullptr;
	}
	return addedPiece;
}

void GameBoard::reduceAllCells()
{
	for (int y = 0; y < BoardSize::ROWS; y++) {
		for (int x = 0; x < BoardSize::COLOMNS; x++) {
			reduceCell(x, y);
		}
	}
}

void GameBoard::reduceCell(int x, int y)
{
	if (!GameBoard::verifyPointRange(x, y)) {
		return;
	}
	GameBoardCell& cell = m_board[x][y];
	if (cell.isEmpty()) {
		return;
	}
	cell.reducePieces();
}

bool GameBoard::verifyPointRange(int x, int y)
{
	if (x < 0|| x > BoardSize::COLOMNS - 1 || y < 0 || y > BoardSize::ROWS - 1) {
		return false;
	}
	return true;
}

bool GameBoard::applyPlayerMoveOperation(Player & player, MoveOperation & operation)
{
	if (!validatePlayerMoveOperation(player, operation)) {
		return false;
	}
	int sourceX = operation.getSourceX();
	int sourceY = operation.getSourceY();
	//The validation above assures us it is not a nullptr
	Piece* piece = getPieceAtLocation(sourceX, sourceY);
	if (piece == nullptr) {
		cerr << "ERROR: piece at (" << sourceX << "," << sourceY << ") was nullptr!!" << endl;
		cerr << "\t should never happen!!" << endl;
		return false;
	}
	GameBoardCell& source = m_board[sourceX][sourceY];
	GameBoardCell& dest = m_board[operation.getDestenationX()][operation.getDestenationY()];
	source.allocate(m_numOfPlayers);
	dest.allocate(m_numOfPlayers);
	if (!source.movePieceToCell(dest, *piece)) {
		//should never happed after all the validations
		cerr << "ERROR: should never happen (failed to move piece)" << endl;
		return false;
	}
	if (this->m_display != nullptr) {
		this->m_display->onLocationUpdated(source, sourceX, sourceY);
		this->m_display->onLocationUpdated(dest, operation.getDestenationX(), operation.getDestenationY());
	}
	return true;
}

bool GameBoard::applyPlayerJokerTransformationOperation(Player & player, MoveOperation & operation)
{
	if (!operation.isJokerTrasformative()) {
		return true;
	}
	if (!validatePlayerJokerTransformationOperation(player, operation)) {
		return false;
	}
	int jokerX = operation.getJokerX();
	int jokerY = operation.getJokerY();
	Piece* joker = getPieceAtLocation(jokerX, jokerY);
	//if the piece died after the last movement
	// ex: if we moved a joker piece, it died then we want to change the 
	// representation of it. Bam!.
	if (joker == nullptr) {
		cerr << "ERROR: received a nullptr joker at (" << jokerX << "," << jokerY << ")" << endl;
		return false;
	}
	joker->setType(operation.getJokerNewRep());
	if (this->m_display != nullptr) {
		this->m_display->onLocationUpdated(m_board[jokerX][jokerY], jokerX, jokerY);
	}
	return true;
}


ostream& operator<<(ostream& out, GameBoard& board) {
	for (int y = 0; y < GameBoard::BoardSize::ROWS; y++) {
		for (int x = 0; x < GameBoard::BoardSize::COLOMNS; x++) {
			if (!GameBoard::isShouldPrint(board.m_board[x][y])) {
				out << (char)(PieceType::EMPTY);
				continue;
			}
			char ch = board.m_board[x][y].getPiece().getType();
			if (board.m_board[x][y].getPiece().isJoker()) {
				ch = PieceType::JOKER;
			}
			if (board.m_board[x][y].getPiece().getPlayer()->getPlayerID() == 2) {
				ch = tolower(ch);
			}
			out << ch;
		}
		out << endl;
	}
	return out;
}

bool GameBoard::isShouldPrint(GameBoardCell & cell)
{
	if (cell.isEmpty()) {
		return false;
	}
	assert(cell.getPiece().getPlayer() != nullptr);
	return (cell.getPiece().getPlayer() != nullptr && (!cell.getPiece().isDead()));
}

