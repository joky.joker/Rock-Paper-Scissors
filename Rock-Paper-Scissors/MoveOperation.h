#pragma once
#include "PieceType.h"

class Piece;

class MoveOperation {
private:
	int m_fromX = 0;
	int m_fromY = 0;
	int m_toX = 0;
	int m_toY = 0;
	int m_jokerX = 0;
	int m_jokerY = 0;
	PieceType m_newRep = PieceType::INVALID;
	bool m_isJokerTransformative = false;
	bool m_isValid = true;
	bool isValidStep() const;
public:
	MoveOperation(int from_x, int from_y, int to_x, int to_y) :
		m_fromX(from_x - 1), m_fromY(from_y - 1), m_toX(to_x - 1), m_toY(to_y - 1) {};
	void setJokerChange(int joker_x, int joker_y, char newRep);
	bool isValidMove() const;
	bool isValidTransformation() const;
	bool isJokerTrasformative() const { return this->m_isJokerTransformative; }
	int getSourceX() { return m_fromX; }
	int getSourceY() { return m_fromY; }
	int getDestenationX() const { return m_toX; }
	int getDestenationY() const { return m_toY; }
	int getJokerX() const { return m_jokerX; }
	int getJokerY() const { return m_jokerY; }
	PieceType getJokerNewRep() const { return m_newRep; }

};