#pragma once
#include "Piece.h"

class GameBoardCell {
	Piece * m_pieces = nullptr;
	int m_capacity = 0;
	int m_size = 0;
public:
	GameBoardCell() = default;
	explicit GameBoardCell(int cellsNumber);
	~GameBoardCell() { delete[] m_pieces; m_pieces = nullptr; }
	GameBoardCell& operator=(const GameBoardCell& cell) = delete;
	GameBoardCell(const GameBoardCell& cell) = delete;
	bool movePieceToCell(GameBoardCell& cell, Piece& piece);
	bool allocate(int cellsNumber);
	bool isEmpty() const { return m_size == 0; }
	bool isPlayerPresent(int playerID);
	void reducePieces();
	int getSize() const { return m_size; }
	Piece * addPiece(const Piece& piece);
	Piece& getPiece();
	void removePiece(Piece& piece);
};