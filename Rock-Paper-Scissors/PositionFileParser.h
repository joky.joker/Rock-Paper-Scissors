#pragma once
#include <fstream>
#include <iostream>
#include "Piece.h"
#include "GameBoard.h"
#include "FileUtil.h"

struct PositionResult {
	Piece piece;
	int x;
	int y;
};

class PositionFileParser {
	
	FileUtil m_fileUtil;
public:
	bool isDone() { return m_fileUtil.isDone(); }
	bool isFailed() { return m_fileUtil.isFailed(); };
	bool openFile(char * file_path) { return m_fileUtil.openFile(file_path); }
	int getLineNumber() { return m_fileUtil.getLineNumber(); }
	PositionResult parseNextPositionPiece();
};