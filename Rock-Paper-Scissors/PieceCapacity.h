#pragma once

enum PieceCap {
	ROCK_CAP = 2,
	PAPER_CAP = 5,
	SCISSORS_CAP = 1,
	BOMB_CAP = 2,
	JOKER_CAP = 2,
	FLAG_CAP = 1,
};