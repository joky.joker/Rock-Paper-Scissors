#include "MoveFileParser.h"
#include "Piece.h"

using namespace std;

MoveOperation MoveFileParser::nextMoveOperation()
{
	if (!this->m_fileUtil.isOpen() || !this->m_fileUtil.isGood()) {
		cerr << "ERROR: move file is not open" << endl;
		//To make sure it will always be invalid Operation.
		return MoveOperation(-1,-1,-1,-1);
	}
	
	m_fileUtil.consumeSpaces();
	m_fileUtil.consumeNewLines();

	int from_x = m_fileUtil.getNextNumber();
	int from_y = m_fileUtil.getNextNumber();
	int to_x = m_fileUtil.getNextNumber();
	int to_y = m_fileUtil.getNextNumber();
	char next;
	

	next = this->m_fileUtil.getNextChar();
	if (next != PieceType::JOKER) {
		this->m_fileUtil.unget();
		m_fileUtil.consumeSpaces();
		return MoveOperation(from_x, from_y, to_x, to_y);
	}
	next = this->m_fileUtil.getNextByte();
	if (next != ':' || this->m_fileUtil.getNextByte() != ' ') {
		cerr << "Missing ': ' after J at line:" << m_fileUtil.getLineNumber() << endl;
		return MoveOperation(-1, -1, -1, -1);
	}
	MoveOperation moveOperation(from_x, from_y, to_x, to_y);
	int joker_x = m_fileUtil.getNextNumber();
	int joker_y = m_fileUtil.getNextNumber();
	char new_rep = m_fileUtil.getNextChar();
	m_fileUtil.consumeSpaces();
	moveOperation.setJokerChange(joker_x, joker_y, Piece::parsePieceType(new_rep));
	return moveOperation;
}

