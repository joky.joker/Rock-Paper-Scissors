#pragma once
#include "Player.h"
#include "CommandLineParams.h"
#include "DisplayManager.h"
#include "GameBoard.h"
#include "PositionFileParser.h"


class Game {
private:
	int m_numOfPlayers;
	int m_numOfRounds;
	Player * m_players = nullptr;
	CommandLineParams m_params;
	DisplayManager m_display;
	GameBoard m_gameBoard;

	Player * playerIDToPlayer(int playerID);
	bool initilizePositiningFile(Player& player);
	bool loadPlayerPositionFile(char * filePath, Player& player);
	bool validateMoveFilesExistance();
	bool validatePositiningFilesExistance();
	bool validateFilesExistance(char * position_file_template);
	void dumpIntoOutputFile(int winner);
public:
	Game(const Game& game) = delete;
	Game& operator=(const Game& game) = delete;
	Game(const CommandLineParams& params, int numOfPlayers, int numOfRounds);
	~Game();
	int getWinner(bool isInitialCheck = false);
	int Run();
	int RunRound();
	int initilizeGamePositions();
	bool validateFilesExistance();
};