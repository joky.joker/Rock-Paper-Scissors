#pragma once
#include "Piece.h"

class PieceTypeCounter {
	int m_num_of_bombs = 0;
	int m_num_of_papers = 0;
	int m_num_of_scissors = 0;
	int m_num_of_jokers = 0;
	int m_num_of_rocks = 0;
	int m_num_of_flags = 0;
	bool validatePiecesNumbersNoFlags();
public:
	bool countPiece(const Piece & piece);
	bool validateAllIncludingFlags();
};