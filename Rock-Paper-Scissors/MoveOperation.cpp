#include "MoveOperation.h"
#include "GameBoard.h"

bool MoveOperation::isValidStep() const
{
	//Note to make sure the total difference in x and y is 1
	// Means it moves a step in either right, left, up or down and not diagonally.
	return abs((m_fromX + m_fromY) - (m_toX + m_toY)) == 1;
}

void MoveOperation::setJokerChange(int joker_x, int joker_y, char newRep)
{
	this->m_jokerX = joker_x - 1;
	this->m_jokerY = joker_y - 1;
	this->m_isJokerTransformative = true;
	this->m_newRep = Piece::parsePieceType(newRep);
}

bool MoveOperation::isValidMove() const
{
	return isValidStep()&& GameBoard::verifyPointRange(this->m_fromX, this->m_fromY) &&
		GameBoard::verifyPointRange(this->m_toX, this->m_toY) && isValidStep();
}

bool MoveOperation::isValidTransformation() const
{
	if (!this->m_isJokerTransformative) {
		return true;
	}
	return GameBoard::verifyPointRange(m_jokerX, m_jokerY) && Piece::isRPSB(this->getJokerNewRep());
}
