#pragma once
#include "MoveOperation.h"
#include "FileUtil.h"

class MoveOperation;
class Player;

class MoveFileParser {
private:
	 FileUtil m_fileUtil;
public:
	MoveFileParser() = default;
	bool isDone() { return m_fileUtil.isDone(); }
	bool isOpen() { return m_fileUtil.isOpen(); }
	bool isFailed() { return m_fileUtil.isFailed(); };
	int getLineNumber() { return m_fileUtil.getLineNumber(); }
	bool openFile(char * file_path) { return m_fileUtil.openFile(file_path); }
	MoveOperation nextMoveOperation();
};