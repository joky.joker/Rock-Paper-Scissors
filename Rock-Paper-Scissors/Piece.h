#pragma once
#include <fstream>
#include <iostream>
#include <cmath>
#include "Player.h"
#include "PieceType.h"
#include "FightResult.h"



class Player;
using namespace std;

class Piece
{
private:
	PieceType m_type = PieceType::EMPTY;
	bool m_isJoker = false;
	bool m_isDead = false;
	Player * m_player = nullptr;
	bool m_isKnownToBeJoker = false;
	PieceType m_lastKnownType = PieceType::UNKNOWN;
public:
	static PieceType parsePieceType(char type);
	Piece() : Piece(PieceType::EMPTY, false, nullptr) {};
	Piece(PieceType type, bool isJoker, Player * player) : 
		m_type(type), m_isJoker(isJoker), m_player(player) {};
	friend ostream& operator<<(ostream& out, const Piece& piece);
	friend bool operator==(const Piece& piece1, const Piece& piece2);
	PieceType getType() const { return m_type; }
	bool isJoker() const { return m_isJoker; }
	bool isDead() const { return m_isDead; }
	Player *  getPlayer() const { return m_player; }

	bool canMove() const;
	void setType(PieceType type) { m_type = type; }
	void setDead(bool isDead) { m_isDead = isDead; }
	void setJoker(bool isJoker) { m_isJoker = isJoker; }
	void setPlayer(Player * player) { m_player = player; }
	void setIsKnownToBeJoker(bool isKnown) { m_isKnownToBeJoker = isKnown; }
	bool isKnownToBeJoker() const { return m_isKnownToBeJoker; }
	PieceType getKnownType() const { return m_lastKnownType; }
	bool doesHavePlayer() const { return m_player != nullptr; }
	void onPieceFaught();
	static bool isRPSB(char type);
	void reset();

	FightResult fight(Piece &p2);
};
