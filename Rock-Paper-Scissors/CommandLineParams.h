#pragma once

class CommandLineParams {
private:
	static const char PARAM_QUIET[];
	static const char PARAM_SHOW_ALL[];
	static const char PARAM_SHOW_ONLY_KNOWN_INFO[];
	static const char PARAM_SHOW[];
	static const char PARAM_DELAY[];

	bool m_presentation = true;
	bool m_showAll = true;
	int m_delay = 50;
	int m_showPlayer = 0;
	bool m_showOnlyKnownInfo = false;
public:
	CommandLineParams() = default;
	static CommandLineParams parseCommandLineParams(int argc, char ** argv);
	bool isPresentationMode() const { return m_presentation; }
	bool isShowAll() const { return m_showAll && isPresentationMode(); }
	bool isShowOnlyKnownInfo() const { return isPresentationMode() && m_showOnlyKnownInfo; }
	int getShow() const;
	int getDelay() const;
};