#include "Player.h"
#include "MoveFileParser.h"

Player::Player()
{
	for (int i = 0; i < SUM_OF_PIECES; i++) {
		this->m_pieces[i] = nullptr;
	}
	for (int i = 0; i < PieceCap::FLAG_CAP; i++) {
		this->m_flags[i] = nullptr;
	}
}

MoveFileParser & Player::getPlayerMoveFile()
{
	if (m_MoveFile.isOpen()) {
		return m_MoveFile;
	}
	char file_name[] = PLAYER_MOVE_FILE_NAME;
	constexpr int NUM_INDEX = 6;
	file_name[NUM_INDEX] = m_playerID + '0';
	this->m_MoveFile.openFile(file_name);
	return m_MoveFile;
}

bool Player::hasLost()
{
	if (this->m_hasLost) {
		return true;
	}

	return hasFlagBeenCaptured() || haveAllMovingPiecesBeenEaten();
}

bool Player::hasFlagBeenCaptured()
{
	bool isAllDead = true;
	for (int i = 0; i < this->m_numOfFlags; i++) {
		Piece * piece = this->m_flags[i];
		isAllDead &= (piece->isDead());
	}

	if (isAllDead) {
		this->lose(Reason::ALL_FLAGS_CAPTURED);
		return true;
	}
	return false;
}

bool Player::haveAllMovingPiecesBeenEaten()
{
	bool isAllDeadOrUnmoveable = true;
	for (int i = 0; i < this->m_numOfPieces; i++) {
		Piece * piece = this->m_pieces[i];
		isAllDeadOrUnmoveable &= (piece->isDead() || !piece->canMove());
	}

	if (isAllDeadOrUnmoveable) {
		this->lose(Reason::ALL_MOVING_PIECES_EATEN);
		return true;
	}
	return false;
}

void Player::lose(LosingReason& reason)
{
	this->m_hasLost = true;
	this->m_reason = reason;
}

bool Player::addPiece(Piece & piece)
{
	if (Piece::parsePieceType(piece.getType()) == PieceType::INVALID) {
		return false;
	}
	if (piece.getType() != PieceType::FLAG && this->m_numOfPieces >= SUM_OF_PIECES) {
		cerr << "ERROR: cant add piece '" << (char)piece.getType() << "' to player " << this->m_playerID;
		cerr << " ,exceeding max capacity" << endl;
		return false;
	} 
	if (piece.getType() == PieceType::FLAG && this->m_numOfFlags >= PieceCap::FLAG_CAP) {
		cerr << "ERROR: cant add flag piece '" << (char)(piece.getType()) << "' to player " << this->m_playerID;
		cerr << " - too many flags" << endl;
		return false;
	}
	if (piece.getType() == PieceType::FLAG) {
		this->m_flags[this->m_numOfFlags++] = &piece;
		return true;
	}
	
	this->m_pieces[this->m_numOfPieces++] = &piece;
	return true;
}

void Player::removePieceFromArray(Piece* arr[], int& size, Piece & piece)
{
	for (int i = 0; i < size; i++) {
		if (&piece == arr[i]) {
			swap(arr[size - 1], arr[i]);
			arr[size - 1] = nullptr;
			size--;
			return;
		}
	}
}
void Player::onPieceRemoved(Piece & piece)
{
	if (piece.getType() == PieceType::FLAG) {
		Player::removePieceFromArray(this->m_flags, this->m_numOfFlags, piece);
	} else {
		Player::removePieceFromArray(this->m_pieces, this->m_numOfPieces, piece);
	}
}

void Player::onPieceMoved(Piece * oldPiece, Piece * newPiece)
{
	if (oldPiece == nullptr || newPiece == nullptr) {
		return;
	}
	//we go only on pieces because flags cant move anyway.
	for (int i = 0; i < m_numOfPieces; i++) {
		if (m_pieces[i] == oldPiece) {
			m_pieces[i] = newPiece;
		}
	}
}

ostream & Player::getWinningReason(ostream & out, const Player & player1, const Player & player2)
{
	LosingReason& reason1 = player1.getLosingReason();
	LosingReason& reason2 = player2.getLosingReason();
	if (reason1.getReason() == reason2.getReason()) {
		if (reason1.getReason() == Reason::BAD_POSITIONING_FILE) {
			out << "Bad Positioning input file for both players - player 1: line " << reason1.getWrongLine();
			out << ", player 2: line " << reason2.getWrongLine() << endl;
			return out;
		}
		if (reason1.getReason() == Reason::BAD_MOVES_FILE) {
			out << "Bad Move input file for both players - player 1: line " << reason1.getWrongLine();
			out << ", player 2: line " << reason2.getWrongLine() << endl;
			return out;
		}
		if (reason1.getReason() == Reason::DIDNT_LOSE) {
			out << "A tie - both Moves input files done without a winner" << endl;
			return out;
		}
		//They are both the same it doesnt matter
		player1.printReasonForPlayer(out);
		return out;
	}

	if (!player1.printReasonForPlayer(out)) {
		player2.printReasonForPlayer(out);
	}

	return out;
}

bool Player::printReasonForPlayer(ostream & out) const
{
	LosingReason& reason = this->getLosingReason();
	switch (reason.getReason())
	{
	case Reason::BAD_POSITIONING_FILE:
		out << "Bad Positioning input file for player " << this->getPlayerID() << " - line " << reason.getWrongLine() << endl;
		break;
	case Reason::ALL_FLAGS_CAPTURED:
		out << "All flags of the opponent are captured" << endl;
		break;
	case Reason::ALL_MOVING_PIECES_EATEN:
		out << "All moving PIECEs of the opponent are eaten" << endl;
		break;
	case Reason::BAD_MOVES_FILE:
		out << "Bad Moves input file for player " << this->getPlayerID() << " - line " << reason.getWrongLine() << endl;
		break;
	default:
		return false;
		break;
	}
	return true;
}
