//#include <vld.h>
#include <iostream>
#include "CommandLineParams.h"
#include "Config.h"
#include "Player.h"
#include "Piece.h"
#include "Game.h"
#include "GameBoard.h"


using namespace std;

void printUsage(char * selfName) {
	cerr << "Usage: " << selfName << endl;
	cerr << "\t The game should be provided for each player a position file and a move file" << endl;
	cerr << "\t called player1.rsp_board and player1.rsp_moves where '1' is the player number" << endl;
	cerr << "Enjoy! :)" << endl;
}

int main(int argc, char ** argv) {
	const CommandLineParams params = CommandLineParams::parseCommandLineParams(argc, argv);
	Game game(params, 2, 1);
	if (!game.validateFilesExistance()) {
		printUsage(argv[0]);
		return 1;
	}
	int winner = game.initilizeGamePositions();
	if (winner != -1) {
		if (winner == 0) {
			std::cout << "No one won" << endl;
		}
		else {
			std::cout << "Player " << winner << " Won!! :)" << endl;
		}
		return 0;
	}

	winner = game.Run();
	if (winner == 0) {
		std::cout << "No one won" << endl;
	}
	else {
		std::cout << "Player " << winner << " Won!! :)" << endl;
	}
	return 0;
}

/*
Samples of the game's stdout:

*/