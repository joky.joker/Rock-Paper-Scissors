#ifdef _WIN32
#include <windows.h>
#endif // WINDOWS

#include "DisplayManager.h"
#include "GameBoard.h"

void DisplayManager::printGameBoard(GameBoard & board)
{
	if (!this->m_params.isPresentationMode()) {
		return;
	}
	
	getCursorXY(m_startingX, m_startingY);
	for (int y = 0; y < GameBoard::BoardSize::ROWS; y++) {
		printLine();
		for (int x = 0; x < GameBoard::BoardSize::COLOMNS; x++) {
			Piece * p = board.getPieceAtLocation(x, y);
			cout << "| ";
			if (p == nullptr) {
				cout << "  ";
				continue;
			}
			printPieceCell(*p);
			cout << " ";
		}
		cout << "|" << endl;
	}
	printLine();
	getCursorXY(m_endingX, m_endingY);

}

void DisplayManager::onLocationUpdated(GameBoardCell & cell, int x, int y)
{
	if (!this->m_params.isPresentationMode()) {
		return;
	}
	moveCursorToPosition(4 * x + m_startingX + 2, 2 * y + 1 + m_startingY);
	printPieceCell(cell.getPiece());
	moveCursorToPosition(m_endingX, m_endingY);
}

void DisplayManager::delay()
{
	Sleep(this->m_params.getDelay());
}

void DisplayManager::printInColor(const char* strToPrint, Color fgColor, Color bgColor)
{
	setColor(fgColor, bgColor);
	cout << strToPrint;
	resetColor();
}

void DisplayManager::printPieceCell(Piece & piece)
{
	if (piece.getPlayer() == nullptr) {
		cout << piece << " ";
		return;
	}
	Color color = (Color)(piece.getPlayer()->getPlayerID() + 2);
	setColor(color);
	printPiece(piece);
	resetColor();
}

void DisplayManager::printPiece(Piece & piece)
{
	if (m_params.isShowOnlyKnownInfo()) {
		printPieceShowOnlyKnownInfo(piece);
		return;
	}
	if (m_params.getShow() != 0) {
		if (piece.getPlayer() == nullptr) {
			cerr << "ERROR: should never happen, attempted to print a playerless piece" << endl;
			printPieceShowAll(piece);
			return;
		}
		if (piece.getPlayer()->getPlayerID() == m_params.getShow()) {
			printPieceShowAll(piece);
			return;
		} else {
			printPieceShowOnlyKnownInfo(piece);
			return;
		}
	}
	if (m_params.isShowAll()) {
		printPieceShowAll(piece);
		return;
	}
}

void DisplayManager::printPieceShowAll(Piece & piece)
{
	char ch = piece.getType();
	if (piece.isJoker()) {
		ch = tolower(ch);
	}
	cout << ch ;
}

void DisplayManager::printPieceShowOnlyKnownInfo(Piece & piece)
{
	char ch = (char)piece.getKnownType();
	if (piece.isKnownToBeJoker()) {
		ch = tolower(ch);
	}
	cout << ch;
}

void DisplayManager::getCursorXY(int& x, int& y)
{
#ifdef _WIN32
	CONSOLE_SCREEN_BUFFER_INFO screenBufferInfo;
	HANDLE hStd = GetStdHandle(STD_OUTPUT_HANDLE);
	if (!GetConsoleScreenBufferInfo(hStd, &screenBufferInfo))
		return;
	x = screenBufferInfo.dwCursorPosition.X;
	y = screenBufferInfo.dwCursorPosition.Y;
#endif // _WIN32
}

void DisplayManager::printLine()
{
	cout << "+";
	for (int i = 0; i < GameBoard::BoardSize::COLOMNS; i++) {
		cout << "---+";
	}
	cout << endl;
}

void DisplayManager::setColor(Color ForgC, Color BackC)
{
#ifdef _WIN32
	WORD wColor = ((BackC & 0x0F) << 4) + (ForgC & 0x0F);
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), wColor);
#endif // _WIN32
}

void DisplayManager::moveCursorToPosition(int x, int y)
{
#ifdef _WIN32
	COORD coord;
	coord.X = x;
	coord.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
#endif // _WIN32
}