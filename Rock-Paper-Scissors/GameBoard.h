#pragma once
#include <fstream>
#include "Piece.h"
#include "Player.h"
#include "Config.h"
#include "MoveOperation.h"
#include "GameBoardCell.h"
#include "DisplayManager.h"

class DisplayManager;

class GameBoard {
private:
	GameBoardCell ** m_board = nullptr;
	Player * m_players = nullptr;
	int m_numOfPlayers = 2;
	DisplayManager * m_display = nullptr;
	
	bool validatePlayerMoveOperation(Player& player, MoveOperation& operation);
	bool validatePlayerJokerTransformationOperation(Player& player, MoveOperation& operation);
	bool validateSourcePieceBelongsToPlayer(Player & player, int x, int y);
	void reduceCell(int x, int y);
	static bool isShouldPrint(GameBoardCell& piece);
public:
	GameBoard& operator=(GameBoard&& other);
	GameBoard(const GameBoard& board) = delete;
	GameBoard& operator=(const GameBoard& board) = delete;
	~GameBoard();
	GameBoard() = default;
	GameBoard(Player * players, DisplayManager* displayManager, int numOfPlayers = 2);
	Piece * initilizePieceAtLocation(Piece& piece, int x, int y);
	Piece * getPieceAtLocation(int x, int y);
	static bool verifyPointRange(int x, int y);
	bool applyPlayerMoveOperation(Player& player, MoveOperation& operation);
	bool applyPlayerJokerTransformationOperation(Player& player, MoveOperation& operation);
	friend ostream& operator<<(ostream& out, GameBoard& board);
	void reduceAllCells();
	enum BoardSize {
		COLOMNS = 10,
		ROWS = 10,
	};
};