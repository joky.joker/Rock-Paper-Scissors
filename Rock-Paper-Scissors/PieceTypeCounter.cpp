#include "PieceTypeCounter.h"
#include "PieceCapacity.h"
#include <iostream>


bool PieceTypeCounter::validatePiecesNumbersNoFlags()
{
	if (m_num_of_scissors > PieceCap::SCISSORS_CAP || m_num_of_bombs > PieceCap::BOMB_CAP ||
		m_num_of_jokers > PieceCap::JOKER_CAP || m_num_of_rocks > PieceCap::ROCK_CAP ||
		m_num_of_papers > PieceCap::PAPER_CAP || m_num_of_flags > PieceCap::FLAG_CAP) {
		return false;
	}
	return true;
}

bool PieceTypeCounter::countPiece(const Piece& piece)
{
	if (piece.isJoker()) {
		m_num_of_jokers++;
		return validatePiecesNumbersNoFlags();
	}

	switch (piece.getType()) {
	case PieceType::SCISSORS: m_num_of_scissors++;
		break;
	case PieceType::BOMB: m_num_of_bombs++;
		break;
	case PieceType::PAPER: m_num_of_papers++;
		break;
	case PieceType::ROCK: m_num_of_rocks++;
		break;
	case PieceType::JOKER: m_num_of_jokers++;
		break;
	case PieceType::FLAG: m_num_of_flags++;
		break;
	default:
		std::cerr << "ERROR: got invalid type " << (char)piece.getType() << std::endl;
		return false;
	}

	return validatePiecesNumbersNoFlags();
	
}

bool PieceTypeCounter::validateAllIncludingFlags()
{
	return validatePiecesNumbersNoFlags() && m_num_of_flags == PieceCap::FLAG_CAP;
}
