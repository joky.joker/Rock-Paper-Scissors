#include "Piece.h"
#include "Config.h"

PieceType Piece::parsePieceType(char type)
{
	bool isValid = (type == PieceType::BOMB || type == PieceType::FLAG ||
		type == PieceType::JOKER || type == PieceType::PAPER
		|| type == PieceType::ROCK || type == PieceType::SCISSORS);
	if (!isValid) {
		return PieceType::INVALID;
	}
	return (PieceType)(type);
}

bool Piece::canMove() const
{
	return m_type == PieceType::ROCK ||
		m_type == PieceType::SCISSORS ||
		m_type == PieceType::PAPER;
}

void Piece::onPieceFaught()
{
	//if we're joker, and we've transformed, and it's not our first trasnformation
	if (isJoker() && m_lastKnownType != m_type && m_lastKnownType != PieceType::UNKNOWN) {
		m_isKnownToBeJoker = true;
	}
	m_lastKnownType = m_type;
}

bool Piece::isRPSB(char representation)
{
	PieceType type = Piece::parsePieceType(representation);
	return type == PieceType::BOMB || type == PieceType::SCISSORS ||
		type == PieceType::ROCK || type == PieceType::PAPER;
}

void Piece::reset()
{
	this->m_type = PieceType::EMPTY;
	this->m_player = nullptr;
	this->m_isJoker = false;
	this->m_isDead = false;
	this->m_isKnownToBeJoker = false;
}

bool operator==(const Piece& piece1, const Piece& piece2) {
	return piece1.m_player == piece2.m_player &&
		piece1.m_type == piece2.m_type &&
		piece1.m_isDead == piece2.m_isDead &&
		piece1.m_isJoker == piece2.m_isJoker &&
		piece1.m_isKnownToBeJoker == piece2.m_isKnownToBeJoker;
		
}
ostream& operator<<(ostream& out, const Piece& piece) {
	out << (char)(piece.getType());
	return out;
}

FightResult Piece::fight(Piece &p2)
{
	if (this->getType() == p2.getType()) {
		return FightResult::BOTH_DIE;
	}
	if (this->getType() == PieceType::BOMB || p2.getType() == PieceType::BOMB) {
		return FightResult::BOTH_DIE;
	}

	if ((this->getType() == PieceType::ROCK && p2.getType() == PieceType::SCISSORS) ||
		(this->getType() == PieceType::SCISSORS && p2.getType() == PieceType::PAPER) ||
		(this->getType() == PieceType::PAPER && p2.getType() == PieceType::ROCK)||
		p2.getType() == PieceType::FLAG) {
		return FightResult::WIN;
	}

	return FightResult::LOSE;
}