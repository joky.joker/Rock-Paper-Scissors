#include "GameBoardCell.h"
#include "FightResult.h"

bool GameBoardCell::movePieceToCell(GameBoardCell & cell, Piece & piece)
{
	bool isMyPiece = false;
	for (int i = 0; i < m_size;i++) {
		if (&m_pieces[i] == &piece) {
			isMyPiece = true;
			break;
		}
	}
	if (!isMyPiece) {
		return false;
	}
	Piece * newPiece = cell.addPiece(piece);
	if (newPiece == nullptr) {
		return false;
	}
	piece.getPlayer()->onPieceMoved(&piece, newPiece);
	//piece.reset();
	this->removePiece(piece);
	cell.reducePieces();
	return true;
}

bool GameBoardCell::allocate(int cellsNumber)
{
	if (m_pieces != nullptr) {
		return false;
	}
	m_capacity = cellsNumber;
	m_pieces = new Piece[m_capacity];
	m_size = 0;
	return true;
}

bool GameBoardCell::isPlayerPresent(int playerID)
{
	//Must not be called with a maybe dead player.
	if (isEmpty()) {
		return false;
	}
	for (int i = 0; i < m_size; i++) {
		Piece& piece = m_pieces[i];
		if (piece.isDead() || piece.getType() == PieceType::INVALID || piece.getPlayer() == nullptr) {
			continue;
		}
		Player * player = piece.getPlayer();
		if (player->getPlayerID() == playerID) {
			return true;
		}
	}
	return false;
}

void GameBoardCell::reducePieces()
{
	//removeDeadPieces();
	//assume there are 2 or less pieces
	if (getSize() <= 1) {
		return;
	}
	Piece& first = m_pieces[0];
	Piece& second = m_pieces[1];

	if (first.getPlayer()->getPlayerID() == second.getPlayer()->getPlayerID()) {
		cerr << "ERROR: Should never happen!" << endl;
		return;
	}

	switch (first.fight(second))
	{
	case FightResult::WIN:
		first.onPieceFaught();
		second.getPlayer()->onPieceRemoved(second);
		this->removePiece(second);
		break;
	case FightResult::LOSE:
		second.onPieceFaught();
		first.getPlayer()->onPieceRemoved(first);
		this->removePiece(first);
		break;
	case FightResult::BOTH_DIE:
		second.getPlayer()->onPieceRemoved(second);
		this->removePiece(second);
		first.getPlayer()->onPieceRemoved(first);
		this->removePiece(first);
		break;
	default:
		cerr << "Error: got unidentified result from fight" << endl;
		break;
	}
}

Piece *  GameBoardCell::addPiece(const Piece & piece)
{
	if (m_pieces == nullptr) {
		return nullptr;
	}
	if (m_capacity == m_size) {
		return nullptr;
	}
	//removeDeadPieces();
	m_pieces[m_size++] = piece;
	return &m_pieces[m_size - 1];
}

Piece & GameBoardCell::getPiece()
{
	return m_pieces[0];
}

void GameBoardCell::removePiece(Piece & piece)
{
	for (int i = 0;i < m_size;i++) {
		if (m_pieces[i] == piece || m_pieces[i].isDead()) {
			if (m_size > 1) {
				//replace the last and the removed piece
				swap(m_pieces[i], m_pieces[m_size - 1]);
				//if the last piece we replaced has a player we will notify it of the movement
				if (m_pieces[i].getPlayer() != nullptr) {
					Player * player = m_pieces[i].getPlayer();
					player->onPieceMoved(&m_pieces[m_size - 1], &m_pieces[i]);
				}
			}
			//m_size cant be 0, because then the loop wouldn't run
			m_pieces[m_size - 1].reset();
			m_size--;
		}
	}
}

//void GameBoardCell::removeDeadPieces()
//{
//	return;
//	for (int i = 0;i < m_size;i++) {
//		//maybe add (m_pieces[i].doesHavePlayer() && m_pieces[i].getPlayer()->hasLost())?
//		if (m_pieces[i].isDead()) {
//			removePiece(m_pieces[i]);
//		}
//	}
//}

GameBoardCell::GameBoardCell(int cellNumber) : m_capacity(cellNumber)
{
	m_pieces = new Piece[m_capacity];
}
