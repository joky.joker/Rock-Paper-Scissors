#include "FileUtil.h"

bool FileUtil::isFileExists(char * file_name)
{
	return std::ifstream(file_name).good();
}

bool FileUtil::isDone()
{
	consumeWhiteSpaces();
	m_file.clear();
	m_file.get();
	m_file.unget();
	return !m_file.good();
}

int FileUtil::consumeChar(char ch)
{
	int consumed = 0;
	char result = ch;
	while (result == ch) {
		result = m_file.get();
		consumed++;
	}
	m_file.unget();
	return --consumed;
}

void FileUtil::consumeWhiteSpaces()
{
	char ch = m_file.get();
	while (ch == ' ' || ch == '\n') {
		if (ch == '\n') {
			m_lineNumber++;
		}
		ch = m_file.get();
	}
	m_file.unget();
}
 
bool FileUtil::isFailed()
{
	return m_file.bad();
}

bool FileUtil::openFile(char * file_path)
{
	this->m_file = std::ifstream(file_path);
	consumeSpaces();
	consumeNewLines();
	return m_file.is_open();
}

char FileUtil::getNextChar()
{
	consumeSpaces();
	return m_file.get();
}

int FileUtil::getNextNumber()
{
	char result = getNextChar();
	//Got to a number! (hopefully)
	if (result > '9' || result <= '0') {
		return -1;
	}
	int number = 0;
	while (result <= '9' && result >= '0') {
		int num = result - '0';
		number *= 10;
		number += num;
		result = m_file.get();
	}
	m_file.unget();
	return number;
}

