#pragma once

enum PieceType {
	ROCK = 'R',
	PAPER = 'P',
	SCISSORS = 'S',
	BOMB = 'B',
	JOKER = 'J',
	FLAG = 'F',
	EMPTY = ' ',
	UNKNOWN = 'U',
	INVALID = '!',
};