#pragma once
#include "Config.h"
#include "Piece.h"
#include "MoveFileParser.h"
#include "LosingReason.h"
#include "PieceCapacity.h"

class Piece;

class LosingReason;

class Player
{
private:
	static constexpr int SUM_OF_PIECES = (PieceCap::BOMB_CAP + PieceCap::JOKER_CAP +
		PieceCap::PAPER_CAP + PieceCap::ROCK_CAP + PieceCap::SCISSORS_CAP);
	int m_playerID = 0;
	int m_score = 0;
	LosingReason m_reason;
	Piece * m_flags[PieceCap::FLAG_CAP];
	Piece * m_pieces[SUM_OF_PIECES];
	MoveFileParser m_MoveFile;
	int m_numOfFlags = 0;
	int m_numOfPieces = 0;
	bool m_hasLost = false;
	static void removePieceFromArray(Piece* arr[], int& size, Piece& piece);
	bool printReasonForPlayer(ostream& out) const;
public:
	Player();
	int getPlayerID() const { return m_playerID; }
	MoveFileParser& getPlayerMoveFile();
	void setPlayerID(int playerID) { m_playerID = playerID; }
	int getScore() { return m_score; }
	void addPoint() { m_score += 1; }
	bool hasLost();
	bool hasFlagBeenCaptured();
	bool haveAllMovingPiecesBeenEaten();
	LosingReason getLosingReason() const { return m_reason; }
	void lose(LosingReason& losingReason);
	void lose(Reason reason) { this->lose(LosingReason(reason)); }
	bool addPiece(Piece& piece);
	void onPieceRemoved(Piece& piece);
	void onPieceMoved(Piece * oldPiece, Piece * newPiece);
	static ostream& getWinningReason(ostream& out, const Player& player1, const Player& player2);
};